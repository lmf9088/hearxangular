import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TotalService {

  sbj = new Subject();

  constructor() { }

  sendTotal(item){
    console.log(item);
    this.sbj.next(item);
  }

  getTotal(){
    return this.sbj.asObservable();
  }
}
