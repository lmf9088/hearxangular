import { Component, OnInit } from '@angular/core';
import { Data } from 'src/app/models/data';
import { ProductService } from "../../../sevices/product.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  product: Data[] = [];

  constructor(private value: ProductService) { }

  ngOnInit(): void {
    this.product = this.value.getData()
  }

}
