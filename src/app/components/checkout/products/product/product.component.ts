import { Component, Input, OnInit } from '@angular/core';
import { Data } from 'src/app/models/data';
import { TotalService } from 'src/app/sevices/total.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input() item: Data;

  constructor(private tot: TotalService) { }

  ngOnInit(): void {
  }

  handleItemToList(){
    this.tot.sendTotal(this.item);
  }

}
