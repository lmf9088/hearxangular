import { QueryValueType } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Data } from 'src/app/models/data';
import { TotalService } from 'src/app/sevices/total.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  totalItems = [];
  totalPrice = 0;

  constructor(private tot: TotalService) {

  }

  ngOnInit(): void {
    this.tot.getTotal().subscribe((item: Data) => {
      this.addToList(item);
    });
  }

  addToList(item: Data) {

    let itemAdded = false;
    for (let i in this.totalItems) {
      if (this.totalItems[i].id === item.id) {
        this.totalItems[i].qty++;
        itemAdded = true;
        break;
      }
    }

    if(!itemAdded){
      this.totalItems.push({ id: item.id, name: item.name, price: item.price, qty: 1 });
    }

    this.totalPrice = 0;
    this.totalItems.forEach(item => {
      this.totalPrice += (item.qty * item.price)
    })
    console.log(this.totalItems.length);
  }

}
