import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/public/header/header.component';
import { FooterComponent } from './components/public/footer/footer.component';
import { NavigationComponent } from './components/public/navigation/navigation.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ProductsComponent } from './components/checkout/products/products.component';
import { CartComponent } from './components/checkout/cart/cart.component';
import { ItemComponent } from './components/checkout/cart/item/item.component';
import { ProductComponent } from './components/checkout/products/product/product.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    CheckoutComponent,
    ProductsComponent,
    CartComponent,
    ItemComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
