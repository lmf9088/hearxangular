export class Data {
    id: number;
    photo: string;
    name: string;
    price: number;

    constructor(id,photo,name,price){
        this.id = id;
        this.photo = photo;
        this.name = name;
        this.price = price;
    }
}
